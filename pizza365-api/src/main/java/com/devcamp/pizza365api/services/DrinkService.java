package com.devcamp.pizza365api.services;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.devcamp.pizza365api.models.CDrink;

@Service
public class DrinkService {
    CDrink drink1 = new CDrink("TRATAC","Trà tắc",10000,null,new Date().getTime(),new Date().getTime());
    CDrink drink2 = new CDrink("COCA","Cocacola",15000,null,new Date().getTime(),new Date().getTime());
    CDrink drink3 = new CDrink("PEPSI","Pepsi",15000,null,new Date().getTime(),new Date().getTime());
    CDrink drink4 = new CDrink("LAVIE","Lavie",5000,null,new Date().getTime(),new Date().getTime());
    CDrink drink5 = new CDrink("TRASUA","Trà sữa trân châu",40000,null,new Date().getTime(),new Date().getTime());
    CDrink drink6 = new CDrink("FANTA","Fanta",15000,null,new Date().getTime(),new Date().getTime());
    public ArrayList<CDrink> getAllDrinks(){
        ArrayList<CDrink> drinkList = new ArrayList<>();
        drinkList.add(drink1);
        drinkList.add(drink2);
        drinkList.add(drink3);
        drinkList.add(drink4);
        drinkList.add(drink5);
        drinkList.add(drink6);
        return drinkList;
    }
}
