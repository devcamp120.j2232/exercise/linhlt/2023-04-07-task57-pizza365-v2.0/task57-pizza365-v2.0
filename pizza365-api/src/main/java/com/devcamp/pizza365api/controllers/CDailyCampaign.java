package com.devcamp.pizza365api.controllers;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CDailyCampaign {
    @GetMapping("/devcamp-date")
    public String getDateViet(@RequestParam(value = "username", defaultValue = "Pizza Lover") String name) {
		DateTimeFormatter dtfVietnam = DateTimeFormatter.ofPattern("EEEE").withLocale(Locale.forLanguageTag("vi"));
		LocalDate today = LocalDate.now(ZoneId.systemDefault());
        String date = dtfVietnam.format(today);
        String campaignString = "";
        if (date.equals("Thứ hai")){
            campaignString = "Mua 1 tặng 1";
        }
        else if (date.equals("Thứ ba")){
            campaignString = "Tặng tất cả khách hàng một phần bánh ngọt";
        }
        else if (date.equals("Thứ tư")){
            campaignString = "Mua Pizza tặng 1 coupon giảm giá Thứ 4 10%";
        }
        else if (date == "Thứ năm"){
            campaignString = "Miễn phí 1 đồ uống khi mua Pizza size L bất kỳ";
        }
        else if (date.equals("Thứ sáu")){
            campaignString = "Tặng tất cả khách hàng một phần salad";
        }
        else if (date.equals("Thứ bảy")){
            campaignString = "Bạn được phục vụ bởi chị chủ xinh đẹp";
        }
        else if (date.equals("Chủ nhật")){
            campaignString = "Mỗi hóa đơn trên 2 triệu được tặng 1 pizza size M";
        }
		return String.format("Hello %s ! Hôm nay %s: %s.", name, date, campaignString);
	}
}
