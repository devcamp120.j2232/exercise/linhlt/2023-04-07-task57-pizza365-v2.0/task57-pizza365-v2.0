package com.devcamp.pizza365api.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365api.models.CDrink;
import com.devcamp.pizza365api.services.DrinkService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CDrinkController {
    @Autowired
    DrinkService drinkService;
    @GetMapping("/drink-list")
    public ArrayList<CDrink> getAllDrinksApi(){
        return drinkService.getAllDrinks();
    }
}
