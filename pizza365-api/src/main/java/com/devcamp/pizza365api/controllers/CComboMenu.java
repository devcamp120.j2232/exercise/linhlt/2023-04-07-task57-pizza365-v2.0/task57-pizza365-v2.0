package com.devcamp.pizza365api.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365api.models.CMenu;
import com.devcamp.pizza365api.services.MenuService;

@RestController
@RequestMapping("/")
@CrossOrigin

public class CComboMenu {
    @Autowired
    MenuService menuService;
    @GetMapping("/combo-menu")
    public ArrayList<CMenu> getAllMenuApi(){
        return menuService.getAllMenu();
    }
}
